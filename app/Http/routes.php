<?php
//use DB;
use App\Comment;
use App\Tour;
Route::get('/', function () {
    if(Auth::check()){
         return view('tours.dashboard');
    }
    else {
         return view('auth.login');
    }
});
Route::group(['middleware' => ['web', 'auth']], function () {
    Route::resource('tour', 'TourController');
    Route::get('/add-tour', function () {
        return view('tours.addtour');
    });
    Route::get('/voucher', function () {
        return view('tours.voucher');
    });
    Route::get('/vouchers', function () {
        $tours = Tour::all();
        return view('tours.vouchers', compact('tours'));
    });
    Route::get('/invoices', function () {
        $tours = Tour::all();
        return view('tours.invoices', compact('tours'));
    });
    Route::get('/invoice', function () {
        return view('tours.invoice');
    });
    Route::get('/dashboard', function () {
        return view('tours.dashboard');
    });
      Route::get('/logout', function () {
       Auth::guard('web')->logout();
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    });
    Route::get('/download/voucher/{file}', ['as' => 'downloadFile', function ($file) {
    $headers = array(
        'Content-Type: application/pdf',
        'Content-Description: File Transfer',
        'Content-Disposition: attachment; filename=' . $file,
        'Content-Transfer-Encoding: binary'
    );
    return response()->download(public_path('/uploads/pdf/vouchers/' . $file), $file, $headers);
}]);
    Route::get('/download/invoice/{file}', ['as' => 'downloadFile', function ($file) {
    $headers = array(
        'Content-Type: application/pdf',
        'Content-Description: File Transfer',
        'Content-Disposition: attachment; filename=' . $file,
        'Content-Transfer-Encoding: binary'
    );
    return response()->download(public_path('/uploads/pdf/invoices/' . $file), $file, $headers);
}]);
});


Route::auth();

Route::get('/home', 'HomeController@index');
