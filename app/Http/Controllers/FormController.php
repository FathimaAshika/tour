<?php

namespace App\Http\Controllers;

use App\Form;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class FormController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('forms.index');
    }
    public function store(Request $request) {
        //check form validation again(SUMBIT BUTTON)
        //Front Liner Validation
        $uploadPath= public_path().'/uploads/pdf';
        $pdf_file_name = createPDF($request, $uploadPath);
        //create entry_id
        $entry_id_explode = explode(".", $pdf_file_name);
        $entry_id = $entry_id_explode[0];
       // Form::where('id', $form->id)->update(['generated_pdf' => $pdf_file_name, 'entry_id' => $entry_id]);
        // Create PDF download alert sessions
        if (isset($pdf_file_name)) {
            $title = 'Success !!!';
            $text = 'Your PDF file has been created. Now you can download it.';
            $type = 'success';
            $btn_text = 'Download PDF';
        } else {
            $title = 'ERROR !!!';
            $text = 'Your PDF file has not been created. Please try again.';
            $type = 'error';
            $btn_text = 'OK';
        }
        Session::flash('msg_title', $title);
        Session::flash('msg_text', $text);
        Session::flash('msg_type', $type);
        Session::flash('msg_btn_text', $btn_text);
        Session::flash('pdf_file_name', $pdf_file_name);

      //  return redirect()->route('form.index');
    }
}

function createPDF($request, $uploadPath) {
    //tick image for nature of buisness (in PDF)
    $n1 = '';
    $n2 = '';
    $n3 = '';
    $n4 = '';
    $n5 = '';
    $n6 = '';
    $n7 = '';
    $n8 = '';
    $n9 = '';
    $n10 = '';
    $n11 = '';
    $n12 = '';
    $n13 = '';
    $n14 = '';
    $n15 = '';

//    switch ($request->natureofbusiness) {
//        case "Fast Moving Consumer Goods(FMCG)":
//            $n1 = '<img height="30" width="30" src="' . public_path() . '/images/tick.png" />';
//            break;
//
//        case "Industrial and Agricultural Products and Services":
//            $n2 = '<img height="30" width="30" src="' . public_path() . '/images/tick.png" />';
//            break;
//
//        case "Consumer Durabiles":
//            $n3 = '<img height="30" width="30" src="' . public_path() . '/images/tick.png" />';
//            break;
//
//        case "Fashion and Clothing":
//            $n4 = '<img height="30" width="30" src="' . public_path() . '/images/tick.png" />';
//            break;
//
//        case "Healthcare":
//            $n5 = '<img height="30" width="30" src="' . public_path() . '/images/tick.png" />';
//            break;
//
//        case "Travel & Leisure":
//            $n6 = '<img height="30" width="30" src="' . public_path() . '/images/tick.png" />';
//            break;
//
//        case "Modern Retail":
//            $n7 = '<img height="30" width="30" src="' . public_path() . '/images/tick.png" />';
//            break;
//
//        case "Telecommunication":
//            $n8 = '<img height="30" width="30" src="' . public_path() . '/images/tick.png" />';
//            break;
//        case "Financial Products & Services":
//            $n9 = '<img height="30" width="30" src="' . public_path() . '/images/tick.png" />';
//            break;
//
//        case "Media":
//            $n10 = '<img height="30" width="30" src="' . public_path() . '/images/tick.png" />';
//            break;
//
//        case "Automotive":
//            $n11 = '<img height="30" width="30" src="' . public_path() . '/images/tick.png" />';
//            break;
//
//        case "Alcohol & Tobacco":
//            $n12 = '<img height="30" width="30" src="' . public_path() . '/images/tick.png" />';
//            break;
//
//        case "IT":
//            $n13 = '<img height="30" width="30" src="' . public_path() . '/images/tick.png" />';
//            break;
//
//        case "Corporate Selling":
//            $n14 = '<img height="30" width="30" src="' . public_path() . '/images/tick.png" />';
//            break;
//
//        case "Insurence":
//            $n15 = '<img height="30" width="30" src="' . public_path() . '/images/tick.png" />';
//            break;
//    }

    //tick for NOMinee role (in PDF)
    $nominatedforcategory_short_name = 'XX';
    $n16 = '';
    $n17 = '';
    $n18 = '';

//    switch ($request->nominatedforcategory) {
//        case "Front Liner":
//            $nominatedforcategory_short_name = "FL";
//            $n16 = '<img height="29" width="29" src="' . public_path() . '/images/tick2.png" />';
//            break;
//        case "Sales Supervisor/Executive":
//            $nominatedforcategory_short_name = "SE";
//            $n17 = '<img height="29" width="29" src="' . public_path() . '/images/tick2.png" />';
//            break;
//
//        case "Territory Manager":
//            $nominatedforcategory_short_name = "TM";
//            $n18 = '<img height="29" width="29" src="' . public_path() . '/images/tick2.png" />';
//            break;
//    }

    // generate pdf number
//    if ($nominatedforcategory_short_name == 'FL') {
//        $total_count = Form::where("category_role", "Front Liner")->count();
//    } else if ($nominatedforcategory_short_name == 'SE') {
//        $total_count = Form::where("category_role", "Sales Supervisor/Executive")->count();
//    } else {
//        $total_count = Form::where("category_role", "Territory Manager")->count();
//    }
//
//
//    if ($total_count == 1) {
//
//        $ttal_count = 1;
//    } else {
//
//        $ttal_count = $total_count;
//    }
    // Generate pdf name
    $pdf_file_name = 'NASCO-2016-' . $nominatedforcategory_short_name . '-' . ('oo');


    // Maximum execution time
    ini_set('max_execution_time', 200);

    require_once base_path('vendor/tecnickcom/tcpdf/tcpdf.php');

    // Extend the TCPDF class to create custom Header and Footer
    class MYPDF extends \TCPDF {

        //Page header
        public function Header() {
            // Logo
        }

        // Page footer
        public function Footer() {
            // Logo
//            $image_file = public_path('image/quotation-footer-logo.png');
//            $this->Image($image_file, 0, 280, 200, 16, 'PNG', 'http://arimaclanka.com/', 'B', false, 100, 'C', false, false, 0, false, false, false);
//      
            }

    }

    // create new PDF document
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Arimac Lanka (Pvt) Ldt');
    $pdf->SetTitle('Form Generator');
    $pdf->SetSubject('Form Generator');
    $pdf->SetKeywords('Form, Generator, PDF');

    // set default header data
    //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
    $pdf->setFooterData(array(0, 64, 0), array(0, 64, 128));

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
   // $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
        require_once(dirname(__FILE__) . '/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    // ---------------------------------------------------------
    // set default font subsetting mode
    $pdf->setFontSubsetting(true);
    $pdf->SetDrawColor(191, 10, 10);

    // Set font
    $pdf->SetFont('times', '', 10, '', true);

    // Add a page
    $pdf->AddPage();

    // Set some content to print
    $html1 = '
<table>
    <tr>
    <td width="160" height="60"><img src=""/></td>
    <td width="348"><h2>Entry NO: ' . $pdf_file_name . '</h2></td>
    <td width="160"><img src=""/></td>
</tr>
</table>
<table border="1" style="width:100%; font-family:Arial;" >
    <tr>
    <th height="30" colspan="4" style="text-align: center;font-size: 15px; font-color:#333333; background-color: #333333; ">ENTRY FORM 2016</th>
  </tr>
  <tr>
    <th height="30" colspan="4" style="text-align: center;font-size: 15px; background-color: #bfbfbf;">COMPANY INFORMATION</th>
  </tr>
  <tr>
    <td height="30" colspan="3" style="font-size: 14px;">1.Name of the company: ' . 'ashika' . '</td>
    <td height="30" rowspan="5" style="font-size: 14px;"><img height="300" width="300" src="' . public_path() . $uploadPath . '"/></td>
  </tr>
  <tr>
    <td height="30" colspan="3" style="font-size: 14px;">2.Business registration no: ' .'test ' . '</td>
    <td height="30" ></td>
  </tr>
  <tr>
    <td height="30"  colspan="3" style="font-size: 14px;">3.Registerd Address: ' . 'kk' . ' </td>
    <td></td>
  </tr>
  <tr>
    <td  height="30" colspan="3"></td>
    <td></td>
  </tr>
  <tr>
    <td height="30" style="font-size: 14px;" >4.Phone: ' . 'lll' . '</td>
    <td height="30" style="font-size: 14px;" >5.E-mail: ' . 'kkkk' . '</td>
    <td height="30" style="font-size: 14px;" >6.Fax: ' . 'ljjjjjjjj' . '</td>
    <td height="30" ></td>
  </tr>
  <tr>
    <td  height="30" style="font-size: 14px;"  colspan="4">Nature of Business:</td>
  </tr>
  <tr>
    <td height="30"  width="285" style="font-size: 14px;">Fast Moving Consumer Goods(FMCG)</td>
    <td  height="30" width="30">' . $n1 . '</td>
    <td height="30"  width="285" style="font-size: 14px;">Financial Products & Services</td>
    <td  height="30" width="30">' . $n9 . '</td>
  </tr>
  <tr>
    <td height="30" style="font-size: 14px;" >Industrial and Agricultural Products and Services</td>
    <td>' . $n2 . '</td>
    <td style="font-size: 14px;">Media</td>
    <td>' . $n10 . '</td>
  </tr>
  <tr>
    <td height="30" style="font-size: 14px;" >Consumer Durables</td>
    <td>' . $n3 . '</td>
    <td style="font-size: 14px;">Automotive</td>
    <td>' . $n11 . '</td>
  </tr>
  <tr>
    <td height="30" style="font-size: 14px;" >Fashion and Clothing</td>
    <td>' . $n4 . '</td>
    <td style="font-size: 14px;">Alcohol & Tobacco</td>
    <td>' . $n12 . '</td>
  </tr>
  <tr>
    <td height="30" style="font-size: 14px;" >Healthcare</td>
    <td>' . $n5 . '</td>
    <td style="font-size: 14px;">IT</td>
    <td>' . $n13 . '</td>
  </tr>
  <tr>
    <td height="30" style="font-size: 14px;" >Travel & Leisure</td>
    <td>' . $n6 . '</td>
    <td style="font-size: 14px;">Corporate Selling</td>
    <td>' . $n14 . '</td>
  </tr>
  <tr>
    <td height="30" style="font-size: 14px;" >Modern Retail</td>
    <td>' . $n7 . '</td>
    <td style="font-size: 14px;">Insurance</td>
    <td>' . $n15 . '</td>
  </tr>
  <tr>
    <td height="30" style="font-size: 14px;" >Telecommunication</td>
    <td>' . $n8 . '</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <th  height="30"  colspan="4" style="font-size: 14px;text-align: center;">NOMINEE INFORMATION</th>
  </tr>
  <tr>
    <td  height="30"  colspan="4" style="font-size: 14px;">Name: ' . 'kkk' . '</td>
  </tr>
  <tr>
    <td  height="30" colspan="4" style="font-size: 14px;">Designation: ' . 'hhhhhhh' . '</td>
  </tr>
  <tr>
    <td height="30"  colspan="2" style="font-size: 14px;">Email ID: ' . 'mmmm' . '</td>
    <td colspan="2" style="font-size: 14px;">MObile NO:  ' . 'eeeeeeeeee' . '</td>
  </tr>
  <tr>
    <td  height="30" colspan="4" style="font-size: 14px;">Nominated For Category</td>
  </tr>
  <tr>
    <td  height="30" width="120" style="font-size: 14px;">Front LIner</td>
    <td  height="30" width="30">' . $n16 . '</td>
    <td width="180" style="font-size: 14px;">Sales Supervisor/Executive</td>
    <td  height="30" width="30">' . $n17 . '</td>
    <td width="235" style="font-size: 14px;">Territory Manager</td>
    <td height="30" width="30">' . $n18 . '</td>
  </tr>
  <tr>
    <td colspan="4" width="630" height="30" style="font-size: 14px;">Name & Designation of the immediate supervisor of the nominee</td>
  </tr>
  <tr>
    <td colspan="4" width="630" height="30" style="font-size: 14px;">' . 'hhhhhh' . '</td>
  </tr>




</table>
';

    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html1, 0, 1, 0, true, '', true);

    // Add a page
    $pdf->AddPage();

    // Set some content to print
    $html2 = '

<table border="1" style="width:100% font-size:30">

    <tr>
    <td height="30" colspan="2" style="text-align: center;font-size: 15px; background-color: #bbbfba;">ENTRY FORM 2016</td>
  </tr>

  <tr>
    <td height="30" colspan="2" style="font-size: 14px;" >Product/Service Brands sold by the nominee</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="text-align: center;font-size: 14px;">' . '' . '</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">No. of sales professionals reporting to the nominee:<br>
         (applicable only for Executives / Supervisors & Territory Managers only</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">' . 'kkk' . '</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">Total value of business under the nominees control</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">' . 'ggg' . '</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">Geographical area or customer segment assigned to the nominee:</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">' . 'kkkk' . '</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">Job functions – Please list the main job functions of the nominee</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">a.  ' . 'kkkkk' . '</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">b. ' . 'kkkkkk' . '</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">c. ' . 'kkkkkkkkkkk' . '</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">d. ' . 'mmmmmmmmm' . '</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">e. ' . 'lllllllll' . '</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">f. ' . 'llllllll'. '</td>
  </tr>

  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">Performance of the nominee for the calendar year ending 31st December, 2014 / 31st March, 2015</td>
  </tr>
  <tr>
    <td height="30" style="font-size: 14px;">Job function</td>
    <td height="30" style="font-size: 14px;">%</td>
  </tr>
  <tr>
    <td height="30" style="font-size: 14px;">set volume</td>
    <td height="30" style="font-size: 14px;">' . 'lllllllllllsssss' . '</td>
  </tr>
  <tr>
    <td height="30" style="font-size: 14px;">set value</td>
    <td height="30" style="font-size: 14px;">' . 'llllllllll' . '</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">Special achievements of the nominee during last year<br>
(Why do you classify this nominee as a top performer in your organization)</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;" >' . 'mmmmmmmmm' . '</td>
  </tr>
</table>
';
    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html2, 0, 1, 0, true, '', true);
    // Add a page
    $pdf->AddPage();
    // Set some content to print
    $html3 = '
<table border="1" style="width:100% font-size:30">
    <tr>
    <td height="30" colspan="2" style="text-align: center;font-size: 15px;background-color: #bfbfbf;">ENTRY FORM 2016</td>
  </tr>
  <tr>
    <th height="30" colspan="2" style="font-size: 14px;" >Signatures</th>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">I certify that,</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">The information contained in this nomination form are true and accurate</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">The nominee has been working in our organization for the entire period under review in the category in which he is nominated</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">The organization will allow the nominee to participate in the interview process as well as the award ceremony</td>
  </tr>
  <tr>
    <td height="30" colspan="2"></td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">Name (Head of the Sales):  ' . 'kkkkk' . '</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">Designation:  ' . 'llllllll' . '</td>
  </tr>
  <tr>
    <td height="30" colspan="2" rowspan="5" style="font-size: 14px;">Signature with stamp</td>
  </tr>
  <tr>
    <td height="30" style="font-size: 14px;"></td>
  </tr>
  <tr>
    <td height="30" style="font-size: 14px;"></td>
  </tr>
  <tr>
    <td height="30" style="font-size: 14px;"></td>
  </tr>
  <tr>
    <td height="30" style="font-size: 14px;"></td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">*Note – Each company should nominate a coordinator to notify all the nominees there respective details, dates and <br> times for NASCO 2015
</td>
  </tr>
  <tr>
    <td height="30" colspan="2" style="font-size: 14px;">Name: ' . 'kkkkkk' . '</td>
  </tr>
  <tr>
    <td height="30" style="font-size: 14px;">Contact no: ' . 'lllll' . '</td>
    <td height="30" style="font-size: 14px;">Email: ' . 'kkkkkkk' . '</td>
  </tr>
</table>
';
    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html3, 0, 1, 0, true, '', true);
    // ---------------------------------------------------------
    // Close and output PDF document
    $pdf_file_name = $pdf_file_name . '.pdf';
    $pdf->Output(public_path() . '/uploads/pdf/' . $pdf_file_name, 'F');
    //============================================================+
    // END OF FILE
    //============================================================+
    return $pdf_file_name;
}
