<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Tour;
use DB;
use Carbon\Carbon;

class TourController extends Controller {

    public function index() {
        $tours = Tour::all();
        return view('tours.index', compact('tours'));
    }

    public function store(Request $request) {
        $mn = $request->market_name;
        $to = $request->tour_operator;
        $dt = Carbon::now();
        $op = str_split($to);
        $x = str_split($dt);
        $y = str_split($mn);
        $tour_op = $op[0] . '' . $op[1];
        $year_code = $x[2] . '' . $x[3];
        $market_name = $y[0] . '' . $y[1];
        $tour = new Tour();
        // dd(mb_strlen($tour->id));
        $tour->tour_number = $market_name . '' . $tour_op . '' . $year_code . '' . $tour->id;
        $tour->tour_operator = $request->tour_operator;
        $tour->travel_agent = $request->travel_agent;
        $tour->client = $request->client;
        $tour->tour_type = $request->tour_type;
        $tour->flight_number = $request->flight_number;
        $tour->arrival_date = $request->arrival_date;
        $tour->departure_date = $request->departure_date;
        $tour->hotel_name = $request->hotel_name;
        $tour->check_in = $request->check_in;
        $tour->check_out = $request->check_out;
        $tour->basis = $request->basis;
        $tour->room_type = $request->room_type;
        $tour->number_of_adult_pax = $request->number_of_adult_pax;
        $tour->number_of_childs_pax = $request->number_of_childs_pax;
        $tour->number_of_rooms = $request->number_of_rooms;
        $tour->reservation_rate = $request->reservation_rate;
        $tour->service_date = $request->service_date;
        $tour->service = $request->service;
        $tour->service_provider = $request->service_provider;
        $tour->number_of_adult_service_pax = $request->number_of_adult_service_pax;
        $tour->number_of_childs_service_pax = $request->number_of_childs_service_pax;
        $tour->service_rate = $request->service_rate;
        $tour->save();
        $digit = mb_strlen($tour->id);
        $sc;
        switch ($digit) {
            case '1' :
                $sc = '000';
                break;
            case '2' :
                $sc = '00';
                break;
            case '3' :
                $sc = '0';
                break;
            default :
                $sc = '';
                break;
        }
        DB::table('tours')
                ->where('tour_id', $tour->id)
                ->update(['tour_number' => $market_name . '-' . $tour_op . '-' . $year_code . '-' . $sc . '' . $tour->id]);
        $t = DB::table('tours')
                ->where('tour_id', $tour->id)
                ->select('tour_number')
                ->get();
        $tour_num = $t[0]->tour_number;
        $uploadInvoicePath = public_path() . '/uploads/pdf/invoices/';
        $uploadVoucherPath = public_path() . '/uploads/pdf/vouchers/';
        $voucher_file_name = createPDF($request, $uploadVoucherPath, $tour, $tour_num);
        $invoice_file_name = createInvoice($request, $uploadInvoicePath, $tour, $tour_num);
        $entry_id_explode = explode(".", $voucher_file_name);
        $entry_id = $entry_id_explode[0];
        if (isset($voucher_file_name)) {
            $title = 'Success !!!';
            $text = 'Your PDF file has been created. Now you can download it.';
            $type = 'success';
            $btn_text = 'Download PDF';
        } else {
            $title = 'ERROR !!!';
            $text = 'Your PDF file has not been created. Please try again.';
            $type = 'error';
            $btn_text = 'OK';
        }
        return redirect('/add-tour');
    }

}

function createPDF($request, $uploadVoucherPath, $tour, $tour_num) {

    $voucher_file_name = $tour_num;
    ini_set('max_execution_time', 200);

    require_once base_path('vendor/tecnickcom/tcpdf/tcpdf.php');

    // Extend the TCPDF class to create custom Header and Footer
    class MYPDF extends \TCPDF {

        //Page header
        public function Header() {
            // Logo
        }

        // Page footer
        public function Footer() {
            
        }

    }

    // create new PDF document
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Ashika');
    $pdf->SetTitle('Form Generator');
    $pdf->SetSubject('Form Generator');
    $pdf->SetKeywords('Form, Generator, PDF');

    // set default header data
    //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
    $pdf->setFooterData(array(0, 64, 0), array(0, 64, 128));

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    // $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
        require_once(dirname(__FILE__) . '/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    // ---------------------------------------------------------
    // set default font subsetting mode
    $pdf->setFontSubsetting(true);
    $pdf->SetDrawColor(191, 10, 10);
    // Set font
    $pdf->SetFont('times', '', 10, '', true);
    // Add a page
    $pdf->AddPage();
    // Set some content to print
    $html1 = '
<div id="id_div">
        <table  class="table table-bordered">
            <tr>
                <td colspan="2"><img src="images/logo.png" alt=""  height="50px" width="200px"></td>
            </tr>
            <tr>             
                <td>Code:</td>
                <td contenteditable>0001</td>
            </tr>
            <tr>
                <td ><hr/></td>
                 <td ><hr/></td>
            </tr>
            <tr>
                <td>Issued Date:</td>
                <td contenteditable>01/01/2016</td>
            </tr>
            <tr>
                <td>Tour No:</td>
                <td contenteditable>MU/AA/16/0001</td>
            </tr>
            <tr>
                <td>Market:</td>
                <td contenteditable>Market name</td>
            </tr>
            <tr>
                <td>Reference to the Conversation With:</td>
                <td contenteditable>Name</td>
            </tr>

            <tr>
                <td>This Reservation is:</td>
                <td contenteditable><b>Res.type</b></td>
            </tr>	
            <tr>
                <td ><b>Reservation Details</b></td>
                 <td ><b>Reservation Details</b></td>
            </tr>

            <tr>
                <td>Name of the Hotel:</td>
                <td contenteditable>Name</td>
            </tr>

            <tr>
                <td>Check-in:</td>
                <td contenteditable>01/01/2016</td>
            </tr>

            <tr>
                <td>Check-out:</td>
                <td contenteditable>01/01/2016</td>
            </tr>

            <tr>
                <td>Meal Plan:</td>
                <td contenteditable>HB</td>
            </tr>

            <tr>
                <td>Room type:</td>
                <td contenteditable>Standard</td>
            </tr>

            <tr>
                <td>Number of Adult Pax:</td>
                <td contenteditable>Number</td>
            </tr>

            <tr>
                <td>Number of Childs Pax:</td>
                <td contenteditable>Number</td>
            </tr>

            <tr>
                <td>Total Rate:</td>
                <td contenteditable>Number</td>
            </tr>
        </table>

    </div>
';

    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html1, 0, 1, 0, true, '', true);

    // Add a page
    $pdf->AddPage();

    // Set some content to print
    $html2 = '
';
    // Print text using writeHTMLCell()
    //$pdf->writeHTMLCell(0, 0, '', '', $html2, 0, 1, 0, true, '', true);
    // Add a page
    //$pdf->AddPage();
    // Set some content to print
    $html3 = '
';
    // Print text using writeHTMLCell()
    //$pdf->writeHTMLCell(0, 0, '', '', $html3, 0, 1, 0, true, '', true);
    // ---------------------------------------------------------
    // Close and output PDF document
    $voucher_file_name = $voucher_file_name . '.pdf';
    $pdf->Output(public_path() . '/uploads/pdf/vouchers/' . $voucher_file_name, 'F');
    //============================================================+
    // END OF FILE
    //============================================================+
    return $voucher_file_name;
}

function createInvoice($request, $uploadInvoicePath, $tour, $tour_num) {

    $invoice_file_name = $tour_num;
    ini_set('max_execution_time', 200);

    require_once base_path('vendor/tecnickcom/tcpdf/tcpdf.php');

    // Extend the TCPDF class to create custom Header and Footer
    class MYPDF1 extends \TCPDF {

        //Page header
        public function Header() {
            // Logo
        }

        // Page footer
        public function Footer() {
            
        }

    }

    // create new PDF document
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Ashika');
    $pdf->SetTitle('Form Generator');
    $pdf->SetSubject('Form Generator');
    $pdf->SetKeywords('Form, Generator, PDF');

    // set default header data
    //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
    $pdf->setFooterData(array(0, 64, 0), array(0, 64, 128));

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    // $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
        require_once(dirname(__FILE__) . '/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    // ---------------------------------------------------------
    // set default font subsetting mode
    $pdf->setFontSubsetting(true);
    $pdf->SetDrawColor(191, 10, 10);
    // Set font
    $pdf->SetFont('times', '', 10, '', true);

    // Add a page
    $pdf->AddPage();
    // Set some content to print
    $html1 = '
<div id="page-wrap">

		<textarea id="header">INVOICE</textarea>
		
		<div id="identity">
		
            <textarea id="address">37, Elibank Road, Colombo 05, Sri Lanka.
			info@exoticleisuretours.com

PHONE - 0094-112-580005 ,0094-112-580009

HOTLINE - 0094-710322323</textarea>

            <div id="logo">

              

              <div id="logohelp">
                <input id="imageloc" type="text" size="50" value="" /><br />
                (max width: 540px, max height: 100px)
              </div>
              <img id="image" src="" alt="logo" />
            </div>
		
		</div>
		
		<div style="clear:both"></div>
		
		<div id="customer">

            <textarea id="customer-title">Customers Name</textarea>

            <table id="meta">
                <tr>
                    <td class="meta-head">Invoice #</td>
                    <td><textarea>000123</textarea></td>
                </tr>
                <tr>

                    <td class="meta-head">Date</td>
                    <td><textarea id="date">December 15, 2009</textarea></td>
                </tr>
                				
				<tr>

                    <td class="meta-head">From</td>
                    <td><textarea id="date">Reservation</textarea></td>
                </tr>
				
				<tr>

                    <td class="meta-head">To</td>
                    <td><textarea id="date">DWQF, SRILANKA</textarea></td>
                </tr>

            </table>
		
		</div>
		
		<table id="items">
		
		  <tr>
		      <th>Item</th>
		      <th>Description</th>
		      <th>Unit Cost</th>
		      <th>Quantity</th>
		      <th>Price</th>
		  </tr>
		  
		  <tr class="item-row">
		      <td class="item-name"><div class="delete-wpr"><textarea>Tasks</textarea><a class="delete" href="javascript:;" title="Remove row">X</a></div></td>
		      <td class="description"><textarea>Monthly update(Nov. 1 - Nov. 30, 2009)</textarea></td>
		      <td><textarea class="cost">$650.00</textarea></td>
		      <td><textarea class="qty">1</textarea></td>
		      <td><span class="price">$650.00</span></td>
		  </tr>
		  
		  <tr class="item-row">
		      <td class="item-name"><div class="delete-wpr"><textarea>Products</textarea><a class="delete" href="javascript:;" title="Remove row">X</a></div></td>

		      <td class="description"><textarea>main domain and several subdomains</textarea></td>
		      <td><textarea class="cost">$75.00</textarea></td>
		      <td><textarea class="qty">3</textarea></td>
		      <td><span class="price">$225.00</span></td>
		  </tr>
		  
		  <tr id="hiderow">
		    <td colspan="5"><a id="addrow" href="javascript:;" title="Add a row">Add a row</a></td>
		  </tr>
		  
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Subtotal</td>
		      <td class="total-value"><div id="subtotal">$875.00</div></td>
		  </tr>
		  <tr>

		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Total</td>
		      <td class="total-value"><div id="total">$875.00</div></td>
		  </tr>
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Amount Paid</td>

		      <td class="total-value"><textarea id="paid">$0.00</textarea></td>
		  </tr>
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line balance">Balance Due</td>
		      <td class="total-value balance"><div class="due">$875.00</div></td>
		  </tr>
		
		</table>
		
		<div id="terms">
		  <h5>Terms</h5>
		  <textarea>Thank You; we really appreciate your business!</textarea>
		  <textarea>We do expect payment within 21days, so please process this invoice within that time. There will be a 1.5% interest charge per month on late invoices.</textarea>
		</div>
	

';
    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html1, 0, 1, 0, true, '', true);
    // Add a page
    $pdf->AddPage();
    // Set some content to print
    $html2 = '';
    $html3 = '';
    $invoice_file_name = $invoice_file_name . '.pdf';
    $pdf->Output(public_path() . '/uploads/pdf/invoices/' . $invoice_file_name, 'F');
    //============================================================+
    // END OF FILE
    //============================================================+
    return $invoice_file_name;
}
