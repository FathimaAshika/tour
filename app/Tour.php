<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model {

    protected $fillable = [
        'tour_number',
        'tour_operator',
        'travel_agent',
        'client',
        'tour_type',
        'flight_number',
        'arrival_date',
        'departure_date',
        'hotel_name',
        'check_in',
        'check_out',
        'basis',
        'room_type',
        'number_of_adult_pax',
        'number_of_childs_pax',
        'number_of_rooms',
        'reservation_rate',
        'service_date',
        'service',
        'service_provider',
        'number_of_adult_service_pax',
        'number_of_childs_service_pax',
        'service_rate'
    ];

}
