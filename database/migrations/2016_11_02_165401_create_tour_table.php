<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('tours', function (Blueprint $table) {
            $table->increments('tour_id');
            $table->string('tour_number');
            $table->string('tour_operator');
            $table->string('travel_agent');
            $table->string('client');
            $table->string('tour_type');
            $table->string('flight_number');
            $table->string('arrival_date');
            $table->string('departure_date');
            $table->string('hotel_name');
            $table->string('check_in');
            $table->string('check_out');
            $table->string('basis');
            $table->string('room_type');
            $table->string('number_of_adult_pax');
            $table->string('number_of_childs_pax');
            $table->string('number_of_rooms');
            $table->string('reservation_rate');
            $table->string('service_date');
            $table->string('service');
            $table->string('service_provider');

            $table->string('number_of_adult_service_pax');

            $table->string('number_of_childs_service_pax');
            $table->string('service_rate');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('tours');
    }

}
