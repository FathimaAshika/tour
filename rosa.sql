-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2016 at 05:36 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rosa`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_11_02_165401_create_tour_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tours`
--

CREATE TABLE IF NOT EXISTS `tours` (
  `tour_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tour_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tour_operator` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `travel_agent` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tour_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `flight_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arrival_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `departure_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hotel_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `check_in` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `check_out` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `basis` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `room_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number_of_adult_pax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number_of_childs_pax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number_of_rooms` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reservation_rate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `service_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `service` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `service_provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number_of_adult_service_pax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number_of_childs_service_pax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `service_rate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tour_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tours`
--

INSERT INTO `tours` (`tour_id`, `tour_number`, `tour_operator`, `travel_agent`, `client`, `tour_type`, `flight_number`, `arrival_date`, `departure_date`, `hotel_name`, `check_in`, `check_out`, `basis`, `room_type`, `number_of_adult_pax`, `number_of_childs_pax`, `number_of_rooms`, `reservation_rate`, `service_date`, `service`, `service_provider`, `number_of_adult_service_pax`, `number_of_childs_service_pax`, `service_rate`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ch-cc-1', 'Carolina', 'ashika67', '78', 'Transport Only', 'EK345', '12-3-2016', '17-3-2016', 'king', '12-3-2016', '17-3-2016', 'FB', 'Luxery', '2', '2', '3', '3', '17-3-2016', 'order0', 'hsbc', '2', '3', '5', NULL, '2016-11-10 11:04:34', '2016-11-10 11:04:34');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
