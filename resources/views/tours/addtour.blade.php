@extends('app')
@section('content')
<div class="container">
    {!! Form:: open(['url'=>'tour']) !!}
    <fieldset>
        <legend><span class="number">1.</span> &nbsp;Tour Information</legend>
        <tr>
            <td><label>Market Name</label></td>
            <td>
                <select class="form-control" name="market_name" >
                    <option selected="true" disabled="disabled">Select Here</option>                      
                    <option>MIDDLE-EAST</option>
                    <option>CHINA</option>
                    <option>INDIA</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><label>Tour Operator</label></td>
            <td>
                <select class="form-control" name="tour_operator" >
                    <option selected="true" disabled="disabled">Select Here</option>                      
                    <option>CAROLINA</option>
                    <option>MIA</option>
                    <option>MALCUM</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><label>Travel Agent</label></td>
            <td><input type='text' name='travel_agent' class='form-control'></td>
        </tr>
        <tr>
            <td><label>Client</label></td>
            <td><input type='text' name='client' class='form-control'></td>
        </tr>
        <tr>
            <td><label>Tour Type</label></td>
            </br>
            <td> 
                <input class="form-check-input" type="radio" name="tour_type" value="Hotel Booking Only" >Hotel Booking Only</input>
                </br>
                <input class="form-check-input" type="radio" name="tour_type" value="Transport Only" >Transport Only</input>
                </br>
                <input class="form-check-input" type="radio" name="tour_type" value="Tour Only" >Tour Only</input>
                </br>
                <input class="form-check-input" type="radio" name="tour_type" value="Excursion" >Excursion</input>
            </td>
        </tr>
    </fieldset>
    </br>
    </br>
    <fieldset>
        <legend><span class="number">2.</span>&nbsp;Reservation Details</legend>				
        <tr>
            <td><label>Flight No</label></td>
            <td><input type='text' name='flight_number' class='form-control' placeholder="EK349" ></td>
        </tr>

        <tr>
            <td><label>Arrival Date</label></td>
            <td><input type='date' name='arrival_date' class='form-control'></td>
        </tr>

        <tr>
            <td><label>Departure Date</label></td>
            <td><input type='date' name='departure_date' class='form-control'></td>
        </tr>

        <tr>
            <td><label>Hotel Name</label></td>
            <td><input type='text' name='hotel_name' class='form-control' ></td>
        </tr>

        <tr>
            <td><label>Check-In</label></td>
            <td><input type='date' name='check_in' class='form-control'></td>
        </tr>

        <tr>
            <td><label>Check-Out</label></td>
            <td><input type='date' name='check_out' class='form-control'></td>
        </tr>

        <tr>
            <td><label>Basis</label></td>
            </br>
            <td>
                <input class="form-check-input" type="radio" name="basis" value="RO" >&nbsp;RO</input>&nbsp;
                <input class="form-check-input" type="radio" name="basis" value="BB" >&nbsp;BB</input>&nbsp;
                <input class="form-check-input" type="radio" name="basis" value="FB" >&nbsp;FB</input>&nbsp;
                <input class="form-check-input" type="radio" name="basis" value="AI" >&nbsp;HB</input>&nbsp;
                <input class="form-check-input" type="radio" name="basis" value="HB" >&nbsp;HB</input>&nbsp;
                <input class="form-check-input" type="radio" name="basis" value="HB+" >&nbsp;HB+</input>
            </td>
        </tr>
        </br>
        </br>
        </br>	
        <tr>
            <td><label>Room Type</label></td>
            <td>
                <select class="form-control" name="room_type">
                    <option selected="true" disabled="disabled" >Select Here</option>
                    <option>Luxery</option>
                    <option>Single</option>
                    <option>Double</option>
                    <option>Triple</option>
                    <option>Other</option>
                </select>
            </td>
        </tr>

        <tr>
            <td><label>Number of Adut Pax</label></td>
            <td><input type='number' name='number_of_adult_pax' class='form-control'></td>
        </tr>

        <tr>
            <td><label>Number of Child Pax</label></td>
            <td><input type='number' name='number_of_childs_pax' class='form-control'></td>
        </tr>

        <tr>
            <td><label>Number of Rooms</label></td>
            <td><input type='number' name='number_of_rooms' class='form-control'></td>
        </tr>

        <tr>
            <td><label>Reservation Rates</label></td>
            <td><input type='number' name='reservation_rate' class='form-control'></td>
        </tr>

    </fieldset>		

    <fieldset>
        <legend><span class="number">3.</span>&nbsp;Additonal Services Details</legend>		
        <tr>
            <td><label>Service Date</label></td>
            <td><input type='date' name='service_date' class='form-control'></td>
        </tr>

        <tr>
            <td><label>Service</label></td>
            <td><input type='text' name='service' class='form-control'></td>
        </tr>

        <tr>
            <td><label>Service Provider</label></td>
            <td><input type='text' name='service_provider' class='form-control'></td>
        </tr>

        <tr>
            <td><label>Number of Adult Service Pax</label></td>
            <td><input type='number' name='number_of_adult_service_pax' class='form-control'></td>
        </tr>

        <tr>
            <td><label>Number of Childs Service Pax</label></td>
            <td><input type='number' name='number_of_childs_service_pax' class='form-control'></td>
        </tr>

        <tr>
            <td><label>Service Rate</label></td>
            <td><input type='number' name='service_rate' class='form-control'></td>
        </tr>	<br>
        <tr>
            <td colspan="2">
                <button type="submit" class="btn btn-primary" name="btn-save" id="btn-save">
                    <span class="glyphicon glyphicon-plus"></span> Save this Record
                </button>  
            </td>
        </tr>
    </fieldset> 
    {!! Form:: close() !!}
</div>
@endsection