@extends('app')
@section('content')
<div class="container">
    <table cellspacing="0" width="100%" id="example" class="table table-striped table-hover table-responsive">
        <thead>
            <tr>
                <th>Serial No</th>
                <th>Tour Number</th>
                <th>Tour Operator</th>
                <th>Travel Agent</th>
                <th>Client</th>
                <th>Tour Type</th>
                <th>Flight Number</th>
                <th>Arrival Date</th>
                <th>Departure Date</th>
                <th>Hotel Name</th>
                <th>Check-In</th>
                <th>Check-Out</th>
                <th>Basis</th>
                <th>Room Type</th>		
                <th>Num.of Adult Pax</th>
                <th>Num.of Childs Pax</th>
                <th>Num.of Rooms</th>
                <th>Reservation Rate</th>
                <th>Service Date</th>
                <th>Service</th>
                <th>Service Provider</th>
                <th>Num.of.Adult Service Pax</th>
                <th>Num.of.Childs Service Pax</th>
                <th>Service Rate</th>		
                <th>edit</th>
                <th>delete</th>
            </tr>
        </thead>
        <tbody>
            @foreach($tours as $tour)
                <tr>
                    <td>{{$tour ->tour_id}} </td>
                    <td> {{$tour ->tour_number}} </td>
                    <td> {{$tour ->tour_operator}}</td>
                    <td> {{$tour ->travel_agent}} </td>
                    <td> {{$tour ->client}}</td>
                    <td> {{$tour ->tour_type}}</td>
                    <td> {{$tour ->flight_number}}</td>
                    <td> {{$tour ->arrival_date}}</td>
                    <td> {{$tour ->departure_date}}</td>
                    <td> {{$tour ->hotel_name}}</td>
                    <td> {{$tour ->check_in}}</td>
                    <td> {{$tour ->check_out}}</td>
                    <td> {{$tour ->basis}}</td>
                    <td> {{$tour ->room_type}}</td>
                    <td> {{$tour ->number_of_adult_pax}}</td>
                    <td> {{$tour ->number_of_childs_pax}}</td>
                    <td> {{$tour ->number_of_rooms}}</td>
                    <td> {{$tour ->reservation_rate}}</td>
                    <td> {{$tour ->service_date}}</td>
                    <td> {{$tour ->service}}</td>
                    <td> {{$tour ->service_provider}}</td>
                    <td> {{$tour ->number_of_adult_service_pax}}</td>
                    <td> {{$tour ->number_of_childs_service_pax}}</td>
                    <td> {{$tour ->service_rate}}</td>
                    <td align="center">
                        <a id="<?php  ?>" class="edit-link" href="#" title="Edit">
                            <img src="edit.png" width="20px" />
                        </a></td>

                    <td align="center"><a id="<?php  ?>" class="delete-link" href="#" title="Delete">
                            <img src="delete.png" width="20px" />
                        </a></td>

                </tr>
                @endforeach
        </tbody>
    </table>
</div>
@endsection