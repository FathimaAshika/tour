@extends('welcome')
@section('content')
<h1>All Files</h1>
@foreach($tours as $tour)
<div class="row">
    <h3 > Download <a href="download/{{ $tour->tour_number}}.pdf">{{ $tour->tour_number }}</a> </h3>
</div>
@endforeach
@endsection