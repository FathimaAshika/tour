@extends('app')
@section('content')
<div class="container">
    <div class="row-one">
        <div class="col-md-4 widget">
            <div class="stats-left ">
                <h5>Today</h5>
                <h4>Bookings</h4>
            </div>
            <div class="stats-right">
                <label> 45</label>
            </div>
            <div class="clearfix"> </div>	
        </div>
        <div class="col-md-4 widget states-mdl">
            <div class="stats-left">
                <h5>Today</h5>
                <h4>Visitors</h4>
            </div>
            <div class="stats-right">
                <label> 80</label>
            </div>
            <div class="clearfix"> </div>	
        </div>
        <div class="col-md-4 widget states-last">
            <div class="stats-left">
                <h5>Today</h5>
                <h4>Orders</h4>
            </div>
            <div class="stats-right">
                <label>51</label>
            </div>
            <div class="clearfix"> </div>	
        </div>
        <div class="clearfix"> </div>	
    </div>
    <div class="charts">
        <div class="col-md-4 charts-grids widget">
            <h4 class="title">Monthly Bookings Rate</h4>
            <canvas id="bar" height="300" width="400"> </canvas>
        </div>
        <div class="col-md-4 charts-grids widget states-mdl">
            <h4 class="title">Tour Marketing Rate</h4>
            <canvas id="line" height="300" width="400"> </canvas>
        </div>
        <div class="col-md-4 charts-grids widget">
            <h4 class="title">Total Transactions</h4>
            <canvas id="pie" height="300" width="400"> </canvas>
        </div>
        <div class="clearfix"> </div>


    </div>
    <div class="row">
        <div class="col-md-4 stats-info widget">
            <div class="stats-title">
                <h4 class="title">booking Stats</h4>
            </div>
            <div class="stats-body">
                <ul class="list-unstyled">
                    <li>Tour Only<span class="pull-right">85%</span>  
                        <div class="progress progress-striped active progress-right">
                            <div class="bar green" style="width:85%;"></div> 
                        </div>
                    </li>
                    <li>Transport Only<span class="pull-right">35%</span>  
                        <div class="progress progress-striped active progress-right">
                            <div class="bar yellow" style="width:35%;"></div>
                        </div>
                    </li>
                    <li>Tour & Transport<span class="pull-right">78%</span>  
                        <div class="progress progress-striped active progress-right">
                            <div class="bar red" style="width:78%;"></div>
                        </div>
                    </li>
                    <li>Accomodation Only<span class="pull-right">50%</span>  
                        <div class="progress progress-striped active progress-right">
                            <div class="bar blue" style="width:50%;"></div>
                        </div>
                    </li>
                    <li>Additional Services Only <span class="pull-right">80%</span>  
                        <div class="progress progress-striped active progress-right">
                            <div class="bar light-blue" style="width:80%;"></div>
                        </div>
                    </li>
                    <li class="last">Others <span class="pull-right">60%</span>  
                        <div class="progress progress-striped active progress-right">
                            <div class="bar orange" style="width:60%;"></div>
                        </div>
                    </li> 
                </ul>
            </div>
        </div>
        <div class="col-md-8 stats-info stats-last widget-shadow">
            <table class="table stats-table ">
                <thead>
                    <tr>
                        <th>Tour No</th>
                        <th>Bookings</th>
                        <th>STATUS</th>
                        <th>PROGRESS</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">CH/MM/16/0001</th>
                        <td>Tour only</td>
                        <td><span class="label label-success">In progress</span></td>
                        <td><h5>85% <i class="fa fa-level-up"></i></h5></td>
                    </tr>
                    <tr>
                        <th scope="row">ME/UU/16/0002</th>
                        <td>Tour & Transport</td>
                        <td><span class="label label-warning">New</span></td>
                        <td><h5>35% <i class="fa fa-level-up"></i></h5></td>
                    </tr>
                    <tr>
                        <th scope="row">FR/CC/16/0003</th>
                        <td>Transport Only</td>
                        <td><span class="label label-danger">Overdue</span></td>
                        <td><h5  class="down">40% <i class="fa fa-level-down"></i></h5></td>
                    </tr>
                    <tr>
                        <th scope="row">EU/TY/16/0004</th>
                        <td>Tour Only</td>
                        <td><span class="label label-info">Out of booking</span></td>
                        <td><h5>100% <i class="fa fa-level-up"></i></h5></td>
                    </tr>
                    <tr>
                        <th scope="row">EU/EW/16/0005</th>
                        <td>Transport Only</td>
                        <td><span class="label label-success">In progress</span></td>
                        <td><h5 class="down">10% <i class="fa fa-level-down"></i></h5></td>
                    </tr>
                    <tr>
                        <th scope="row">CH/QS/16/0006</th>
                        <td>Tour Only</td>
                        <td><span class="label label-warning">New</span></td>
                        <td><h5>38% <i class="fa fa-level-up"></i></h5></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="clearfix"> </div>
    </div>
    <div class="row">
        <div class="col-md-8 map widget-shadow">
            <h4 class="title">Visitors Map </h4>
            <div class="map_container"><div id="vmap" style="width: 100%; height: 354px;"></div></div>
            <!--map js-->
            <link href=" {{asset('css/jqvmap.css')}}" rel='stylesheet' type='text/css' />
            <script src="{{asset('js/jquery.vmap.js')}}"></script>
            <script src="{{asset('js/jquery.vmap.sampledata.js')}}" type="text/javascript"></script>
            <script src="{{asset('js/jquery.vmap.world.js')}}" type="text/javascript"></script>
            <script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('#vmap').vectorMap({
        map: 'world_en',
        backgroundColor: '#fff',
        color: '#696565',
        hoverOpacity: 0.8,
        selectedColor: '#696565',
        enableZoom: true,
        showTooltip: true,
        values: sample_data,
        scaleColors: ['#585858', '#696565'],
        normalizeFunction: 'polynomial'
    });
});
            </script>
            <!-- //map js -->
        </div>
        <div class="col-md-4 social-media widget-shadow">
            <div class="wid-social twitter">
                <div class="social-icon">
                    <i class="fa fa-twitter text-light icon-xlg "></i>
                </div>
                <div class="social-info">
                    <h3 class="number_counter bold count text-light start_timer counted">3.1 K</h3>
                    <h4 class="counttype text-light">Tweets</h4>
                </div>
            </div>
            <div class="wid-social google-plus">
                <div class="social-icon">
                    <i class="fa fa-google-plus text-light icon-xlg "></i>
                </div>
                <div class="social-info">
                    <h3 class="number_counter bold count text-light start_timer counted">523</h3>
                    <h4 class="counttype text-light">Circles</h4>
                </div>
            </div>
            <div class="wid-social facebook">
                <div class="social-icon">
                    <i class="fa fa-facebook text-light icon-xlg "></i>
                </div>
                <div class="social-info">
                    <h3 class="number_counter bold count text-light start_timer counted">1.06K</h3>
                    <h4 class="counttype text-light">Likes</h4>
                </div>
            </div>
            <div class="wid-social dribbble">
                <div class="social-icon">
                    <i class="fa fa-dribbble text-light icon-xlg "></i>
                </div>
                <div class="social-info">
                    <h3 class="number_counter bold count text-light start_timer counted">1.6 K</h3>
                    <h4 class="counttype text-light">Subscribers</h4>
                </div>
            </div>
            <div class="wid-social vimeo">
                <div class="social-icon">
                    <i class="fa fa-vimeo-square text-light icon-xlg"> </i>
                </div>
                <div class="social-info">
                    <h3 class="number_counter bold count text-light start_timer counted">2.1 m</h3>
                    <h4 class="counttype text-light">Contacts</h4>
                </div>
            </div>
            <div class="wid-social xing">
                <div class="social-icon">
                    <i class="fa fa-xing text-light icon-xlg "></i>
                </div>
                <div class="social-info">
                    <h3 class="number_counter bold count text-light start_timer counted">2525</h3>
                    <h4 class="counttype text-light">Connections</h4>
                </div>
            </div>
            <div class="wid-social flickr">
                <div class="social-icon">
                    <i class="fa fa-android text-light icon-xlg"></i>
                </div>
                <div class="social-info">
                    <h3 class="number_counter bold count text-light start_timer counted">1221</h3>
                    <h4 class="counttype text-light">Media</h4>
                </div>
            </div>
            <div class="wid-social yahoo">
                <div class="social-icon">
                    <i class="fa fa-yahoo text-light icon-xlg"> Y!</i>
                </div>
                <div class="social-info">
                    <h3 class="number_counter bold count text-light start_timer counted">2525</h3>
                    <h4 class="counttype text-light">Connections</h4>
                </div>
            </div>
            <div class="wid-social rss">
                <div class="social-icon">
                    <i class="fa fa-rss text-light icon-xlg"></i>
                </div>
                <div class="social-info">
                    <h3 class="number_counter bold count text-light start_timer counted">1523</h3>
                    <h4 class="counttype text-light">Subscribers</h4>
                </div>
            </div>
            <div class="wid-social youtube">
                <div class="social-icon">
                    <i class="fa fa-youtube text-light icon-xlg"></i>
                </div>
                <div class="social-info">
                    <h3 class="number_counter bold count text-light start_timer counted">1523</h3>
                    <h4 class="counttype text-light">Subscribers</h4>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <div class="row calender widget-shadow">
        <h4 class="title">Calender</h4>
        <div class="cal1">

        </div>
    </div>
    <div class="clearfix"> </div>
</div>

@endsection
