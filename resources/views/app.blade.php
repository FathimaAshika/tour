<!DOCTYPE HTML>
<html>
    @include('includes.header')
    <body class="cbp-spmenu-push">
        <div class="main-content">
            <!--left-fixed -navigation-->
            @include('includes.sidebar')
            <!--left-fixed -navigation-->
            <!-- header-starts -->
            @include('includes.stick-header')
            <!-- //header-ends -->
            <!-- main content start-->
            <div id="page-wrapper">
                <div class="main-page">
                    @yield('content')
                </div>
            </div>
            <!--footer-->
            <div class="footer">
                <p>&copy; 2016 Exotic Leisure Tours. All Rights Reserved | by <a href="https://exoticleisuretours.com" target="_blank">Exotic Leisure Tours(Pvt)Ltd.</a></p>
            </div>
            <!--//footer-->
        </div>
        @include('scripts.script')
        <!--scrolling js-->
        <script src="{{asset('js/scripts.js')}}" ></script>
        <!--//scrolling js-->
        <!-- Bootstrap Core JavaScript -->
        <script src="{{asset('js/bootstrap.js')}}"></script>
    </body>
</html>